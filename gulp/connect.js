var gulp = require("gulp");
var connect = require('gulp-connect');

// Server
gulp.task('connect', function() {
	connect.server({
		root: ['build/'],
		port: 1357,
		livereload: true
	});
});