var gulp = require("gulp");
var path = require('../package.json').config;

// Fonts first run
gulp.task('fonts-init', function() {
	// Copy fonts
	gulp.src(path.src + 'fonts/**/*.*')
		.pipe(gulp.dest(path.destination + 'fonts/'));
});