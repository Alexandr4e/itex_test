var gulp = require("gulp");
var sourcemaps = require('gulp-sourcemaps');
var less = require("gulp-less");
var notifier = require('node-notifier');
var notify = require("gulp-notify");
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var postcssSVG = require('postcss-svg');
var csso = require('postcss-csso');
var connect = require('gulp-connect');
var watch = require('gulp-watch');
var rename = require('gulp-rename');
var plumber = require('gulp-plumber');
var gutil = require('gulp-util');
var path = require('../package.json').config;
var errorHandler = require('./error-handler');

// Less
gulp.task('less', function() {
	gulp.src(path.src + 'css/main.less')
		.pipe(plumber({errorHandler: errorHandler('Error in [less] task')}))
		.pipe(sourcemaps.init())
		.pipe(less())
		.pipe(postcss([
			postcssSVG({
				paths: ['src/img/svg-icons/'],
			}),
			autoprefixer({
				browsers: ['last 2 versions']
			}),
			// csso()
		]))
		.pipe(rename({ suffix: '.min' }))
		.pipe(notify("Everything is fine!"))
		.pipe(sourcemaps.write(""))
		.pipe(gulp.dest(path.destination + 'css/'));
});

// Watch for less
gulp.task('less:watch', function() {
	watch(path.src + '**/*.less', function(event, cb) {
		gulp.start('less');
		gulp.start('nunjucks');
	});
});