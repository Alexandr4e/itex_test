var notifier = require('node-notifier');
var notify = require("gulp-notify");
var gutil = require('gulp-util');

module.exports = function (title) {
	return function (error) {
		gutil.log([
			gutil.colors.bold.red(title),
			'',
			gutil.colors.magenta('plugin: ') + error.plugin,
			'',
			gutil.colors.magenta('message: ') + error.message,
			''
		].join('\n'));
		notifier.notify({
			'title': title,
			'message': 'Look in terminal'
		});
		this.emit('end');
	};
};