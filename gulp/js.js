var gulp = require("gulp");
var path = require('../package.json').config;
var connect = require('gulp-connect');
var watch = require('gulp-watch');
var fileinclude = require('gulp-file-include');
var uglify = require('gulp-uglifyjs');
var notifier = require('node-notifier');
var notify = require("gulp-notify");
var rename = require('gulp-rename');
var plumber = require('gulp-plumber');
var gutil = require('gulp-util');
var errorHandler = require('./error-handler');

// Js
gulp.task('js', function () {
	gulp.src(path.src + 'scripts/app.js')
		.pipe(plumber({errorHandler: errorHandler('Error in [js] task')}))
		.pipe(fileinclude({
			prefix: "//= "
		}))
		.pipe(uglify())
		.pipe(rename({ suffix: '.min' }))
		.pipe(notify({
			'title': 'JS compilation',
			'message': 'Everything is fine!!'
		}))
		.pipe(gulp.dest(path.destination + 'scripts'));
});

// Watch for js
gulp.task('js:watch', function() {
	watch(path.src + 'scripts/**/*.js', function(event, cb) {
		gulp.start('js');
		gulp.start('nunjucks');
	});
});