var gulp = require("gulp");
var buffer = require('vinyl-buffer');
var merge = require('merge-stream');
var debug = require('gulp-debug');
var gulpsync = require('gulp-sync')(gulp);

// Default task compiles all and starts server, then watches
gulp.task('default', gulpsync.sync([
	'clean',
	[
		'less',
		'nunjucks',
		'js',
		'images-init',
		'fonts-init',
		'favicons-init',
		'connect',
		'less:watch',
		'nunjucks:watch',
		'js:watch'
	]
]));