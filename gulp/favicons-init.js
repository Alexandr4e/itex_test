var gulp = require("gulp");
var path = require('../package.json').config;

// Favicons first run
gulp.task('favicons-init', function() {
	// Copy favicons
	gulp.src(path.src + 'favicons/*.*')
		.pipe(gulp.dest(path.destination));
});