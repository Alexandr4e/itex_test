var gulp = require("gulp");
var path = require('../package.json').config;
var nunjucksRender = require('gulp-nunjucks-render');
var prettify = require('gulp-html-prettify');
var notifier = require('node-notifier');
var uncache = require('gulp-uncache');
var connect = require('gulp-connect');
var watch = require('gulp-watch');
var plumber = require('gulp-plumber');
var gutil = require('gulp-util');
var errorHandler = require('./error-handler');

// Html
gulp.task('nunjucks', function() {
	// Gets .html and .nunjucks files in pages
	return gulp.src(path.src + 'templates/*.+(twig)')
		.pipe(plumber({errorHandler: errorHandler('Error in [nunjuck] task')}))
		.pipe(nunjucksRender({
			path: [path.src + 'templates']
		}))
		.pipe(uncache())
		.pipe(prettify({indent_char: '\t', indent_size: 1}))
		.pipe(gulp.dest(path.destination))
		.pipe(connect.reload());
});

// Watch for twig files
gulp.task('nunjucks:watch', function() {
	watch(path.src + 'templates/**/*.twig', function(event, cb) {
		gulp.start('nunjucks');
	});
});