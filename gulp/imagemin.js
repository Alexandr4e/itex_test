var gulp = require("gulp");
var path = require('../package.json').config;
var imagemin = require('gulp-imagemin');
var imageminPngquant = require('imagemin-pngquant');
var imageminZopfli = require('imagemin-zopfli');
var clean = require('gulp-clean');


// Images optimization
gulp.task('imagemin', function() {
	var prefix = '123';
	gulp.src(path.src + 'img/unopt/*.*')
		.pipe(imagemin([
			imageminPngquant({
				speed: 1,
				quality: 98
			}),
			imageminZopfli({
				more: true
			}),
			imagemin.jpegtran({
				progressive: true,
				arithmetic: true
			}),
			imagemin.svgo({
				plugins: [{
					removeDoctype: true
				}, {
					removeTitle: true
				}, {
					removeViewBox: true
				}, {
					removeComments: true
				}, {
					removeDesc: {
						removeAny: true
					}
				}, {
					cleanupNumericValues: {
						floatPrecision: 2
					}
				}, {
					convertColors: {
						names2hex: false,
						rgb2hex  : false
					}
				}, {
					cleanupIDs: {
						prefix: prefix + '-',
						minify: true
					}
				}]
			}),
		], {
			// verbose: true,
			optimizationLevel: 5,
			progressive: true,
			interlaced: true
		}))
		.pipe(gulp.dest(path.src + 'img/opt/'));

	gulp.src(path.src + 'img/unopt/*.*')
		.pipe(clean());
});