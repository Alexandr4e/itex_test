//TO-DO
(function() {
	var $clear = $('.js-clear'),
		$input = $('#input'),
		$delete = $('.js-delete'),
		$add = $('.js-add'),
		$list = $('#list'),
		$title = $('.js-text');

	$clear.click(function(){
		$input.val('');
	});

	$add.click(function(){
		var $t = $(this),
			text = $input.val();

		$list.append('<li class="todo__item">' +
			'<p class="todo__text js-text">text</p>' +
			'<button class="todo__btn todo__btn_edit"></button>' +
			'<button class="todo__btn todo__btn_delete js-delete"></button>' +
			'</li>');

		$input.val('');

	});

	$delete.click(function(){
		var $t = $(this),
			$item = $t.parent();
		$item.remove();
	});

})();