// MAP
(function() {
	function initMap() {
		var centerLatLng = new google.maps.LatLng(56.8641704, 60.61899126);
		var mapOptions = {
			center: centerLatLng,
			zoom: 15
		};
		var map = new google.maps.Map(document.getElementById("map"), mapOptions);
		var marker = new google.maps.Marker({
			position: centerLatLng,
			map: map,
			title: "Текст всплывающей подсказки"
		});
	}

	google.maps.event.addDomListener(window, "load", initMap);
})();