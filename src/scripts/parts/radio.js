//RADIO
(function() {
	var radio = '.js-radio',
		$price = $('#price');

	$(radio).iCheck({
		radioClass: 'iradio',
		increaseArea: '20%'
	});

	$('input').on('ifChecked', function(event){

		var $t = $(this);

		$price = $price.html($t.data("price"));
	});

})();