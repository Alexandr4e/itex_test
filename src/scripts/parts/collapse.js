(function() {
	var $collapse = $('.js-collapse');

	$collapse.click(function() {
		var $t = $(this),
			$collapseBlock = $t.parent().children(".catalog__collapse-main"),
			$collapseArrow = $t.children(".catalog__collapse-arrow");

		$collapseBlock.toggleClass('catalog__collapse-main_show');
		$collapseArrow.toggleClass('catalog__collapse-arrow_show');

	});

})();